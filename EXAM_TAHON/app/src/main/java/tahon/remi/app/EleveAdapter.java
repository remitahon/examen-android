package tahon.remi.app;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class EleveAdapter extends RecyclerView.Adapter<EleveAdapter.MyViewHolder> {

    List<Eleve> Eleves;
    private Context context;

    public Context getContext() {
        return context;
    }

    public EleveAdapter(List<Eleve>MEleves, Context mcontext){
        Eleves = MEleves;
        context = mcontext;
    }

    @NonNull
    @Override
    public EleveAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.eleve, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EleveAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.display(Eleves.get(i));

    }

    @Override
    public int getItemCount() {
        return Eleves.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nom;
        private TextView prenom;
        private TextView mail;
        private  TextView phone;
        private Context context;
        private itemClickListener ItemClickListener;
        private ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageview);
            prenom = (TextView) itemView.findViewById(R.id.Prenom);
            nom = (TextView) itemView.findViewById(R.id.Nom);
            mail = (TextView) itemView.findViewById(R.id.mail);
            phone = (TextView) itemView.findViewById(R.id.phone);
            context = getContext();
            itemView.setOnClickListener(this);


        }

        public void display(Eleve eleve){
            prenom.setText(eleve.getPrenom());
            nom.setText(eleve.getName());
            mail.setText(eleve.getAdresse());
            phone.setText(eleve.getPhone());
            Glide.with(context).load(eleve.getPicture()).into(imageView);
        }

        void setItemClickListener (itemClickListener ItemClickListener){
            this.ItemClickListener = ItemClickListener;
        }


        @Override
        public void onClick(View view) {



        }
    }
}
