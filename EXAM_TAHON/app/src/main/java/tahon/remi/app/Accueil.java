package tahon.remi.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static tahon.remi.app.retrofitservice.ENDPOINT;

public class Accueil extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private List<Eleve> eleves;
    private EleveAdapter eleveAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        mRecyclerView = (RecyclerView) findViewById(R.id.myRecyclerView);
        eleves = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        retrofit.create(retrofitservice.class);

        retrofitservice RetrofitService = retrofit.create(retrofitservice.class);

        RetrofitService.listExample().enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                
                eleves.add(new Eleve(response.body().getName().getFirst(), response.body().getName().getLast(), response.body().getPhone(),
                        response.body().getAddress().getStreet(),response.body().getEmail(), response.body().getLinkedin(),
                        response.body().getPicture(), response.body().getAddress().getZip().toString(), response.body().getAddress().getState(),
                        response.body().getAddress().getCity()));

            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Toast.makeText(Accueil.this,"ERROR", Toast.LENGTH_SHORT).show();

            }
        });

        eleveAdapter = new EleveAdapter(eleves, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setAdapter(eleveAdapter);




    }
}
