package tahon.remi.app;

public class Eleve {
    private String name;
    private String prenom;
    private String phone;
    private String adresse;
    private String mail;
    private String linkedin;
    private String picture;
    private String zip;
    private String state;
    private String city;

    public Eleve(String name, String prenom, String phone, String adresse, String mail, String linkedin, String picture, String zip, String state, String city) {
        this.name = name;
        this.prenom = prenom;
        this.phone = phone;
        this.adresse = adresse;
        this.mail = mail;
        this.linkedin = linkedin;
        this.picture = picture;
        this.zip = zip;
        this.state = state;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
