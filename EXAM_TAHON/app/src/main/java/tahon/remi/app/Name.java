package tahon.remi.app;

import java.util.HashMap;
import java.util.Map;

public class Name {

    private String last;
    private String first;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
